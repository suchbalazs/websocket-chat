package com.such.chat.controller;

import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.such.chat.model.ChatMessage;

import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ChatController {

    private ArrayList rooms = new ArrayList<>(Arrays.asList("public", "bishop"));
    private SimpMessagingTemplate template;

    public ChatController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }

    @MessageMapping("/greetings")
    @SendToUser("/queue/greetings")
    public ChatMessage greet(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        ChatMessage responseMessage = new ChatMessage();
        responseMessage.setType(ChatMessage.MessageType.PRIVATE);
        responseMessage.setContent("This one only you " + chatMessage.getSender() + " can see.");
        return responseMessage;
    }

    @PostMapping("/room/{name}")
    public ResponseEntity createRoom(@PathVariable String name) {
        rooms.add(name);
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setType(ChatMessage.MessageType.ADD_ROOM);
        chatMessage.setContent(name);
        this.template.convertAndSend("/topic/room/added", chatMessage);
        return ResponseEntity.created(URI.create("/room/" + name)).build();
    }

    @GetMapping("/room")
    public ResponseEntity getRooms() {
        return ResponseEntity.ok(rooms);
    }

    @DeleteMapping("/room/{name}")
    public ResponseEntity deleteRoom(@PathVariable String name) {
        rooms.remove(name);
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setType(ChatMessage.MessageType.REMOVE);
        chatMessage.setContent(name);
        this.template.convertAndSend("/topic/room/remove", chatMessage);
        return ResponseEntity.noContent().build();
    }

    @MessageMapping("/chat/{roomName}")
    public void sendMessageInRoom(@DestinationVariable String roomName, @Payload ChatMessage chatMessage,
                                  SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        this.template.convertAndSend("/topic/" + roomName, chatMessage);
    }

}
