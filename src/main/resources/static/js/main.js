'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
var currentRoom = 'public';
var rooms;

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    username = document.querySelector('#name').value.trim();

    if(username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
    getRooms()
}


function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);
    stompClient.subscribe('/topic/room/remove', onRoomChanges);
    stompClient.subscribe('/topic/room/added', onRoomChanges);
    stompClient.subscribe('/user/queue/greetings', onGreetingsReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )
    stompClient.send("/app/greetings",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');

    document.getElementById("room-input").addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        var xhr = new XMLHttpRequest();
        xhr.open("POST", 'http://localhost:8080/room/' + event.currentTarget.value, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send();
        event.currentTarget.value = '';
      }
    });
}

function generateRoomNodes() {
    var roomNode = document.getElementById('roomArea');
    roomNode.innerHTML = '';
    rooms.forEach(room => {
        roomNode.innerHTML += "<li>" + (room !== 'public' ? "<span onclick=\"removeRoom('" + room + "')\" class=\"remove\">x</span>" : "") + "<span onclick=\"changeRoom('" + room + "')\">" + (currentRoom === room ? "<b>" : "") + room + (currentRoom === room ? "</b>" : "") + "</span></li>";
    });
}

function removeRoom(room) {
    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', 'http://localhost:8080/room/' + room, true);
    xhr.send();
}

function changeRoom(room) {
    var index = Object.keys(stompClient.subscriptions).find(key => stompClient.subscriptions[key] === onMessageReceived)

    stompClient.unsubscribe(index);
    stompClient.send('/topic/' + currentRoom,
             {},
             JSON.stringify({sender: username, type: 'LEAVE'})
         )
    currentRoom = room;
    generateRoomNodes();
    messageArea.innerHTML = '';
    stompClient.subscribe('/topic/' + currentRoom, onMessageReceived);
    stompClient.send('/topic/' + currentRoom,
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )
}

function getRooms() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState != 4) return;

        if (this.status == 200) {
            rooms = JSON.parse(this.responseText);
            generateRoomNodes();
        }
    };

    xhr.open('GET', 'http://localhost:8080/room', true);
    xhr.send();
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    var messageContent = messageInput.value.trim();

    if (messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };

        stompClient.send("/app/chat/" + currentRoom, {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}

function onRoomChanges(payload) {
    var message = JSON.parse(payload.body);

    if (message.type === 'REMOVE') {
        const index = rooms.indexOf(message.content);
        if (index > -1) {
            rooms.splice(index, 1);
        }
        if (currentRoom === message.content) {
            changeRoom('public');
        } else {
            generateRoomNodes();
        }
    } else if (message.type === 'ADD_ROOM') {
        rooms.push(message.content);
        generateRoomNodes();
    }
}

function onGreetingsReceived(payload) {
    onMessageReceived(payload);
}

function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if (message.type === 'PRIVATE') {
        messageElement.classList.add('event-message');
    } else if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}

usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)
